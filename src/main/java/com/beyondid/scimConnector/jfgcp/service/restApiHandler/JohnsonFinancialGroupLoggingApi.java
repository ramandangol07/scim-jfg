package com.beyondid.scimConnector.jfgcp.service.restApiHandler;

import com.beyondid.scimConnector.jfgcp.service.implHttpHelper.urlbuild.HttpService;
import com.beyondid.scimConnector.jfgcp.util.ApplicationConstants;
import com.beyondid.scimConnector.jfgcp.util.JsonConversionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Component
@PropertySource("classpath:application.properties")
public class JohnsonFinancialGroupLoggingApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(JohnsonFinancialGroupLoggingApi.class);

	@Autowired
	private JsonConversionUtil jsonConversionUtil;

	@Autowired
	private ApplicationConstants applicationConstants;

	@Autowired
	@Qualifier("johnsonFinancialLoggingRestCall")
	private HttpService johnsonLoggingServiceRestApiHttpService;

	 @Autowired
	 private Environment env;

	 public Map auditLogging(String activityId,String activityDescription){
	 	String payload = auditPayload(activityId, activityDescription);
	 	String url = applicationConstants.JOHNSON_LOGGING_BASEURL + applicationConstants.JOHNSON_LOGGING_AUDIT;

	 	String response = johnsonLoggingServiceRestApiHttpService.doRequest(url, HttpMethod.POST, payload).getBody();

	 	Map auditResponse = jsonConversionUtil.convertStringtoMap(response);
	 	return auditResponse;
	 }

	public Map errorLogging(String errorId, String exceptionCategory,String errorSource, String errorType,String errorCode, String errorMessage,String stackTrace){
		String payload = errorPayload(errorId,exceptionCategory, errorSource, errorType, errorCode, errorMessage, stackTrace);
		String url = applicationConstants.JOHNSON_LOGGING_BASEURL + applicationConstants.JOHNSON_LOGGING_AUDIT;

		String response = johnsonLoggingServiceRestApiHttpService.doRequest(url, HttpMethod.POST, payload).getBody();

		Map auditResponse = jsonConversionUtil.convertStringtoMap(response);
		return auditResponse;
	}


	private String auditPayload(String activityId,String activityDescription){
	 	Map auditPayload = new HashMap();
	 	Map auditEventsRealTile = new HashMap();
		auditEventsRealTile.put("auditId","1");
		auditEventsRealTile.put("appUser","SCIM-CONNECTOR");
	 	auditEventsRealTile.put("location",InetAddress.getLoopbackAddress().getHostAddress());
		auditEventsRealTile.put("role","SCIM-CONNECTOR");
		auditEventsRealTile.put("workflowId","abc");
		auditEventsRealTile.put("screenId","abc");
		auditEventsRealTile.put("activityId",activityId);
		auditEventsRealTile.put("activityDescription", activityDescription);
		auditEventsRealTile.put("auditDetailId","1");
		auditEventsRealTile.put("auditDate",LocalDate.now());
		auditEventsRealTile.put("createDate",LocalDate.now());
		auditEventsRealTile.put("uuid", UUID.randomUUID().toString());
		auditEventsRealTile.put("sourceSystem","FIS");
		auditEventsRealTile.put("sourceId","1");
		auditEventsRealTile.put("applicationName","SCIM CONNECTOR");

	 	auditPayload.put("auditEventsRealtime",auditEventsRealTile);

	 	return jsonConversionUtil.converMaptoString(auditPayload);
	}

	private String errorPayload(String errorId, String exceptionCategory,String errorSource, String errorType,String errorCode, String errorMessage,String stackTrace){
		Map errorPayload = new HashMap();
		Map errorRealTime = new HashMap();
		errorRealTime.put("errorId",errorId);
		errorRealTime.put("exceptionCategory",exceptionCategory);
		errorRealTime.put("applicationName","SCIM_CONNECTOR");
		errorRealTime.put("timeStamp", LocalDate.now());
		errorRealTime.put("errorSource",errorSource);
		errorRealTime.put("errorType",errorType);
		errorRealTime.put("errorCode",errorCode);
		errorRealTime.put("errorMessage",errorMessage);
		errorRealTime.put("severity","P1");
		errorRealTime.put("functionalArea","Mule");
		errorRealTime.put("incidentId","1");
		errorRealTime.put("serverAddress", InetAddress.getLoopbackAddress().getHostAddress());
		errorRealTime.put("endpoint","Mule");
		errorRealTime.put("incidentRequired","Yes");
		errorRealTime.put("errorDateTime", LocalDateTime.now());
		errorRealTime.put("uuid", UUID.randomUUID().toString());
		errorRealTime.put("sourceId","Yes");
		errorRealTime.put("stackTrace",stackTrace);


		errorPayload.put("errorRealtime",errorRealTime);

		return jsonConversionUtil.converMaptoString(errorPayload);
	}


}
