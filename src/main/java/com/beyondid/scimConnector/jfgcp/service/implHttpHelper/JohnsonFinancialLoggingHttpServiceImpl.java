package com.beyondid.scimConnector.jfgcp.service.implHttpHelper;

import com.beyondid.scimConnector.jfgcp.config.BeyondRestBuilder;
import com.beyondid.scimConnector.jfgcp.service.implHttpHelper.urlbuild.HttpService;
import com.beyondid.scimConnector.jfgcp.util.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("johnsonFinancialLoggingRestCall")
public class JohnsonFinancialLoggingHttpServiceImpl implements HttpService {

    @Autowired(required = true)
    @Qualifier("johnsonLoggingAuthService")
    public RestTemplate johnsonLoggingAuthService;

    private HttpHeaders headers;

    @Autowired
    private ApplicationConstants applicationConstants;

    @Autowired
    private BeyondRestBuilder beyondRestBuilder;

    @PostConstruct
    @Override
    public void initHeaders() {

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List list = new ArrayList();
        list.add(MediaType.APPLICATION_JSON);
        headers.setAccept(list);
        headers.set("client_id", applicationConstants.JOHNSON_LOGGING_CLIENT_ID);
        headers.set("client_secret", applicationConstants.JOHNSON_LOGGING_CLIENT_SECRET);
    }

    @Override
    public ResponseEntity<String> doRequest(String url, HttpMethod method, String requestPayload) {
        HttpEntity<String> entity;

        if (requestPayload == null){
            entity = new HttpEntity(null, headers);
        }
        else{
            entity = new HttpEntity(requestPayload, headers);
        }
        return johnsonLoggingAuthService.exchange(url, method, entity, String.class, 100);
    }


    @Override
    public ResponseEntity<String> doRequest(String url, HttpMethod method, String requestPayload, String immutableId) {
        return null;
    }
}
