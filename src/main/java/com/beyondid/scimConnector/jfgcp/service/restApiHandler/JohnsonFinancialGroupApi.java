package com.beyondid.scimConnector.jfgcp.service.restApiHandler;

import com.beyondid.scimConnector.jfgcp.service.implHttpHelper.urlbuild.HttpService;
import com.beyondid.scimConnector.jfgcp.util.ApplicationConstants;
import com.beyondid.scimConnector.jfgcp.util.JsonConversionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
@PropertySource("classpath:application.properties")
public class JohnsonFinancialGroupApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(JohnsonFinancialGroupApi.class);

	@Autowired
	private JsonConversionUtil jsonConversionUtil;

	@Autowired
	private ApplicationConstants applicationConstants;

	@Autowired
	@Qualifier("johnsonFinancialRestCall")
	private HttpService johnsonServiceRestApiHttpService;

	 @Autowired
	 private Environment env;


	public Map creatMember(Map user, String immutableId) {
		Map nonCustomer = new HashMap();
		nonCustomer.put("non-customer", user);

		String payload = jsonConversionUtil.converMaptoString(nonCustomer);
//		LOGGER.info(payload);
		String url = applicationConstants.BASE_URL + applicationConstants.CREATE_MEMBER_URL ;
		String response = johnsonServiceRestApiHttpService.doRequest(url, HttpMethod.POST, payload, immutableId).getBody();

		Map createResponse = jsonConversionUtil.convertStringtoMap(response);

		return createResponse;

	}

	public Map updateMember(Map user, String immutableId, String jfgId) {
		Map nonCustomer = new HashMap();
		nonCustomer.put("non-customer", user);

		String payload = jsonConversionUtil.converMaptoString(nonCustomer);

		String url = applicationConstants.BASE_URL + applicationConstants.CREATE_MEMBER_URL + "/" + jfgId;
		String response = johnsonServiceRestApiHttpService.doRequest(url, HttpMethod.PUT, payload, immutableId).getBody();

		Map createResponse = jsonConversionUtil.convertStringtoMap(response);

		return createResponse;

	}






}
