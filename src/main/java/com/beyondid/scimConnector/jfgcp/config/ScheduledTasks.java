package com.beyondid.scimConnector.jfgcp.config;

import com.beyondid.scimConnector.jfgcp.Application;
import com.beyondid.scimConnector.jfgcp.service.restApiHandler.OktaApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    @Autowired
    private OktaApi oktaApi;

    @Scheduled(cron = "0 0 7 * * *")
    public void scheduleFixedApiCallForActive(){
        //Check user in okta
         oktaApi.checkExistingMember("scimtestuser@mailinator.com");
        LOGGER.info("Scheduler API Call to Active token");
    }
}
